//Atividade extra herança

#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"

using namespace std;

int main(int argc, char ** argv) {
	
	Aluno aluno_4;
	Aluno * aluno_5;

	Pessoa aluno_1;
	Pessoa * aluno_2;
	Pessoa * aluno_3;
	
	aluno_1.setNome("Bruno");
	aluno_1.setTelefone("555-4444");
	aluno_1.setIdade("14");

	aluno_2 = new Pessoa(); 

	aluno_2->setNome("Maria");
	aluno_2->setTelefone("333-5555");
	aluno_2->setIdade("54");

	aluno_3 = new Pessoa("Joao","35","222-5555");

	cout << "Alunos:"<< endl;
	cout << "Nome\tIdade\tTelefone" << endl;
	cout << aluno_1.getNome() << "\t" << aluno_1.getIdade() << "\t" << aluno_1.getTelefone() << endl;
	cout << aluno_2->getNome() << "\t" << aluno_2->getIdade() << "\t" << aluno_2->getTelefone() << endl;
	cout << aluno_3->getNome() << "\t" << aluno_3->getIdade() << "\t" << aluno_3->getTelefone() << endl;
	cout

	Professor professor_1;
	Professor * professor_2;
	Professor * professor_3;

	professor_1.setNome("Tiago");
	professor_1.setTelefone("303-8888");
	professor_1.setIdade("43");

	professor_2 = new pessoa();
	
	professor_2->setNome("João");
	professor_2->setTelefone("555-3333");
	professor_2->setIdade("37");

	professor_2 = new pwssoa("Carlos", "444-9090", "53");

	cout << "Professores:" << endl;
	cout << "Nome\tIdade\tTelefone" << endl;
	cout << professor_1.getNome() << "\t" << professor_1.getIdade() << "\t" << professor_1.getTelefone() << endl;
	cout << professor_2->getNome() << "\t" << professor_2->getIdade() << "\t" << professor_2->getTelefone() << endl;
	cout << professor_3->getNome() << "\t" << professor_3->getIdade() << "\t" << professor_3->getTelefone() << endl;
	cout

	
	delete(aluno_2);
	delete(aluno_3);
	delete(professor_2);
	delete(professor_3);

}
