#include <iostream>
#include "aluno.hpp"

Aluno::Aluno()
{
	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula(0);
	setCreditos(0);
	setSemestre(0);
	setIra(0);
}

Aluno::Aluno(string nome, string idade, string telefone, int matricula, int quantidadeDeCreditos, int semestre, float ira)
{
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setCreditos(quantidadeDeCreditos);
	setSemestre(semestre);
	setIra(ira);
}

int Aluno::getMatricula()
{
	return matricula;
}
void Aluno::setMatricula(int matricula)
{
	this->matricula = matricula;
}
int Aluno::getCreditos()
{
	return quantidadeDeCreditos;
}
void Aluno::setCreditos(int quantidadeDeCreditos)
{
	this->quantidadeDeCreditos = quantidadeDeCreditos;
}
int Aluno::getSemestre()
{
	return semestre;
}
void Aluno::setSemestre(int semestre)
{
	this->semestre = semestre;
}
float Aluno::getIra()
{
	return float;
}
void Aluno::setIra(float ira)
{
	this->ira = ira;
}

