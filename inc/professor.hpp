#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

class Professor: public Pessoa
{
	private: 
		string curso;
		string disciplina;
		int engresso;
		string formacao ;
	public:
		void Aluno();
		void Aluno(string nome, string idade, string telefone, string curso, string disciplina, int engresso, string formacao);
		string getCurso();
		void setCurso(string curso);
		string getDisciplina();
		void setDisciplina(string disciplina);
		int getEngresso();
		void setEngresso(int engresso);
		string getFormacao();
		void setFormacao(string formacao);
};

#endif
